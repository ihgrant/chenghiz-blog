---
path: "/2015-11-15"
date: "2015-11-15"
title: "untitled"
tags: ["blog"]
---

It's been a long time since I published anything other than a portfolio on the web, but that ends today. I've built this blog with Ghost and Buster and it's hosted in Github Pages under a domain name I own. This is great for me because I don't have to worry about exposing an admin interface to the web, everything is static so it loads quickly, and making new posts is still easy because I can fire up Ghost on my machine, write a new post, then compile and publish it to Github.

I ran into a few pitfalls while working on the setup so I thought I'd mention them here:

* create your Github Pages repo first, and do it on the Github site! All you need to do is create a new repo called <username>.github.io. Mine's ihgrant.github.io.
* Once you have created the repo, install ghost on your machine locally. You'll end up with a directory full of folders like content, core, and so on. Clone your github pages repo next to these, into a directory called static, like so: git clone https://github.com/<username>/<username>.github.io.git static
* Installing buster can be confusing if you're not familiar with Python or its package manager (like me). Fortunately, although OS X does come with a recent version of Python installed, it does not have pip, Python's package manager installed, even though if you installed Python yourself, pip would come with it. You can, however, get pip with Homebrew - and that's what I did.
* at this point, when you run buster generate from your current directory (the one you installed ghost into) it'll populate that new static folder with the files for your static blog.
* Once that's done, all you need to do is run buster deploy to commit those files to the repo and push it up to Github!

Special thanks to Akshit Khurana (https://github.com/axitkhurana/buster) for Buster, and Stefan Scherer (https://stefanscherer.github.io/setup-ghost-for-github-pages/) and LED Technica (http://ledtechnica.com/free-ghost-hosting-on-github-pages/) for giving me extra info to figure out what I was doing wrong. There are a fair number of pieces to slot together!

The theme is original and can be cloned or downloaded here: https://github.com/ihgrant/bl-ggn-theme. I'll be working on it off and on, I'm sure. There are still some rough edges, and I'd like to add syntax highlighting for code samples as well.
