import React from "react";
import PropTypes from "prop-types";
import Link from "gatsby-link";
import Helmet from "react-helmet";

import "../css/typography.css";
import "prismjs/themes/prism-solarizedlight.css";

export default class Template extends React.Component {
    static propTypes = {
        children: PropTypes.func
    };

    render() {
        const { location } = this.props;
        const isRoot = location.pathname === "/";

        return (
            <div>
                <Helmet
                    title="chenghiz.net"
                    meta={[
                        { name: "description", content: "Sample" },
                        { name: "keywords", content: "sample, something" },
                        { name: "viewport", content: "width=device-width" }
                    ]}
                />
                <div className="content">
                    <div style={{ display: "flex" }}>
                        <div className="color heading nav-column">
                            <h1
                                style={{
                                    margin: 0,
                                    fontSize: isRoot ? `2.5rem` : `2rem`,
                                    padding: ".25rem"
                                }}
                            >
                                <Link
                                    className="h-card"
                                    to="/"
                                    style={{
                                        color: "white",
                                        textDecoration: "none"
                                    }}
                                >
                                    BLØGGN
                                </Link>
                            </h1>
                        </div>
                        <div className="gutter" />
                        <div style={{ flex: 2 }} />
                    </div>
                    {this.props.children()}
                </div>
            </div>
        );
    }
}
