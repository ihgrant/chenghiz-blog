import React from "react";
import Helmet from "react-helmet";
import BackIcon from "react-icons/lib/fa/chevron-left";
import ForwardIcon from "react-icons/lib/fa/chevron-right";

import Link from "../components/Link";
import Tags from "../components/Tags";

import "../css/blog-post.css";

export default function Template({ data, pathContext }) {
    const { markdownRemark: post } = data;
    const { next, prev } = pathContext;
    return (
        <div style={{ display: "flex" }}>
            <div className="nav-column" />
            <div className="gutter" />
            <main style={{ flex: 2 }}>
                <Helmet title={`chenghiz.net - ${post.frontmatter.title}`} />
                <article className="blog-post">
                    <h1 className="title">{post.frontmatter.title}</h1>
                    <span className="date">{post.frontmatter.date}</span>
                    <div
                        className="blog-post-content"
                        dangerouslySetInnerHTML={{ __html: post.html }}
                    />
                    <Tags list={post.frontmatter.tags || []} />
                    <div className="navigation">
                        {prev && (
                            <Link className="link prev" to={prev.frontmatter.path}>
                                <BackIcon /> {prev.frontmatter.title}
                            </Link>
                        )}
                        {next && (
                            <Link className="link next" to={next.frontmatter.path}>
                                {next.frontmatter.title} <ForwardIcon />
                            </Link>
                        )}
                    </div>
                </article>
            </main>
        </div>
    );
}

export const pageQuery = graphql`
    query BlogPostByPath($path: String!) {
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            html
            frontmatter {
                date(formatString: "MMMM DD, YYYY")
                path
                tags
                title
            }
        }
    }
`;
