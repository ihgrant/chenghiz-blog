import React from "react";
import GatsbyLink from "gatsby-link";
import "../css/nav.css";

export default function Nav(props) {
    const postLinks = [
        <li className="primary">
            <a className="u-url" href="https://chenghiz.net">
                Home
            </a>
        </li>,
        <li className="primary">
            <a href="https://www.github.com/ihgrant" rel="me">
                Github
            </a>
        </li>,
        <li className="primary">
            <a href="https://www.flickr.com/photos/chenghiz/" rel="me">
                Flickr
            </a>
        </li>
    ].concat(
        props.posts.map(post => {
            return (
                <li key={post.node.frontmatter.path}>
                    <GatsbyLink to={post.node.frontmatter.path}>
                        {post.node.frontmatter.title}
                    </GatsbyLink>
                </li>
            );
        })
    );
    return (
        <nav className="h-card">
            <ul>{postLinks}</ul>
        </nav>
    );
}
